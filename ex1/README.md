# Scope of the folder
This folder contains all the stuff for the exercise 1 of computational physics exam.

## Contents
Description of the files and folders:

- **data/** containts intermidiate files;

- **Exercise__1.ipynb** exploratory code;

- **img/**  output images for the report;

- **Snakefile** python makefile;

- **src/** codes;

- **tex/** report files.

- 
